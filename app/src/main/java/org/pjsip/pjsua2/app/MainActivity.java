/* $Id: MainActivity.java 5022 2015-03-25 03:41:21Z nanang $ */
/*
 * Copyright (C) 2013 Teluu Inc. (http://www.teluu.com)
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 */
package org.pjsip.pjsua2.app;

import android.content.IntentFilter;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.app.Activity;
import android.content.Intent;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.view.Gravity;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.widget.Toast;

import java.util.HashMap;

import org.pjsip.pjsua2.*;

public class MainActivity extends Activity
        implements Handler.Callback, MyAppObserver {
    public static MyApp app = null;
    public static MyAccount account = null;
    public static AccountConfig accCfg = null;
    public static MyBroadcastReceiver receiver = null;
    public static IntentFilter intentFilter = null;

    private String lastRegStatus = "";

    private final Handler handler = new Handler(this);

    public class MSG_TYPE {
        public final static int INCOMING_CALL = 1;
        public final static int CALL_STATE = 2;
        public final static int REG_STATE = 3;
        public final static int BUDDY_STATE = 4;
        public final static int CALL_MEDIA_STATE = 5;
        public final static int CHANGE_NETWORK = 6;
    }

    private class MyBroadcastReceiver extends BroadcastReceiver {
        private String conn_name = "";

        @Override
        public void onReceive(Context context, Intent intent) {
            if (isNetworkChange(context))
                notifyChangeNetwork();
        }

        private boolean isNetworkChange(Context context) {
            boolean network_changed = false;
            ConnectivityManager connectivity_mgr =
                    ((ConnectivityManager) context.getSystemService(
                            Context.CONNECTIVITY_SERVICE));

            NetworkInfo net_info = connectivity_mgr.getActiveNetworkInfo();
            if (net_info != null && net_info.isConnectedOrConnecting() &&
                    !conn_name.equalsIgnoreCase("")) {
                String new_con = net_info.getExtraInfo();
                if (new_con != null && !new_con.equalsIgnoreCase(conn_name))
                    network_changed = true;

                conn_name = (new_con == null) ? "" : new_con;
            } else {
                if (conn_name.equalsIgnoreCase(""))
                    conn_name = net_info.getExtraInfo();
            }
            return network_changed;
        }
    }

    private HashMap<String, String> putData(String uri, String status) {
        HashMap<String, String> item = new HashMap<String, String>();
        item.put("uri", uri);
        item.put("status", status);
        return item;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        if (app == null) {
            app = new MyApp();
            app.init(this, getFilesDir().getAbsolutePath());
        }

        if (app.accList.size() == 0) {
            accCfg = new AccountConfig();
            accCfg.setIdUri("sip:localhost");
            accCfg.getNatConfig().setIceEnabled(true);
            accCfg.getVideoConfig().setAutoTransmitOutgoing(true);
            accCfg.getVideoConfig().setAutoShowIncoming(true);
            account = app.addAcc(accCfg);
        } else {
            account = app.accList.get(0);
            accCfg = account.cfg;
        }

        String[] from = {"uri", "status"};
        int[] to = {android.R.id.text1, android.R.id.text2};

        if (receiver == null) {
            receiver = new MyBroadcastReceiver();
            intentFilter = new IntentFilter(
                    ConnectivityManager.CONNECTIVITY_ACTION);
            registerReceiver(receiver, intentFilter);
        }

        final EditText etId = (EditText) findViewById(R.id.editTextId);
        final EditText etReg = (EditText) findViewById(R.id.editTextRegistrar);
        final EditText etProxy = (EditText) findViewById(R.id.editTextProxy);
        final EditText etUser = (EditText) findViewById(R.id.editTextUsername);
        final EditText etPass = (EditText) findViewById(R.id.editTextPassword);
        final Button regButton = (Button) findViewById(R.id.registerButton);

        etId.setText(accCfg.getIdUri());
        etReg.setText(accCfg.getRegConfig().getRegistrarUri());
        StringVector proxies = accCfg.getSipConfig().getProxies();
        if (proxies.size() > 0)
            etProxy.setText(proxies.get(0));
        else
            etProxy.setText("");
        AuthCredInfoVector creds = accCfg.getSipConfig().getAuthCreds();
        if (creds.size() > 0) {
            etUser.setText(creds.get(0).getUsername());
            etPass.setText(creds.get(0).getData());
        } else {
            etUser.setText("");
            etPass.setText("");
        }

        regButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
//                if (lastRegStatus.length() != 0) {
//                    TextView tvInfo = (TextView) findViewById(R.id.textViewInfo);
//                    tvInfo.setText("Last status: " + lastRegStatus);
//                }

                String acc_id = etId.getText().toString();
                String registrar = etReg.getText().toString();
                String proxy = etProxy.getText().toString();
                String username = etUser.getText().toString();
                String password = etPass.getText().toString();

                accCfg.setIdUri(acc_id);
                accCfg.getRegConfig().setRegistrarUri(registrar);

                AuthCredInfoVector creds = accCfg.getSipConfig().getAuthCreds();
                creds.clear();
                if (username.length() != 0) {
                    creds.add(new AuthCredInfo("Digest", "*", username, 0, password));
                }

                StringVector proxies = accCfg.getSipConfig().getProxies();
                proxies.clear();
                if (proxy.length() != 0) {
                    proxies.add(proxy);
                }

                /* Enable ICE */
                accCfg.getNatConfig().setIceEnabled(true);

                /* Finally */
                lastRegStatus = "";
                try {
                    account.modify(accCfg);
                } catch (Exception e) {
                    showToast(e.getMessage());
                }
            }
        });
    }

    @Override
    public boolean handleMessage(Message m) {
        if (m.what == 0) {

            app.deinit();
            finish();
            Runtime.getRuntime().gc();
            android.os.Process.killProcess(android.os.Process.myPid());

        } else if (m.what == MSG_TYPE.REG_STATE) {

            String msg_str = (String) m.obj;
            lastRegStatus = msg_str;
//            ((TextView) findViewById(R.id.textViewInfo)).setText(lastRegStatus);
            showToast(msg_str);
        }
        else if (m.what == MSG_TYPE.CHANGE_NETWORK) {
            app.handleNetworkChange();
        }
        else {
            /* Message not handled */
            return false;
        }

        return true;
    }

    public void showToast(String msg) {
        Toast toast = Toast.makeText(getApplicationContext(), msg, Toast.LENGTH_SHORT);
        toast.setGravity(Gravity.TOP | Gravity.CENTER, 0, 0);
        toast.show();
    }

    /*
     * === MyAppObserver ===
     *
     * As we cannot do UI from worker thread, the callbacks mostly just send
     * a message to UI/main thread.
     */

    public void notifyRegState(int code, String reason, long expiration) {
        String msg_str = "";
        if (expiration == 0)
            msg_str += "Unregistration";
        else
            msg_str += "Registration";

        if (code / 100 == 2)
            msg_str += " successful";
        else
            msg_str += " failed: " + reason;

        Message m = Message.obtain(handler, MSG_TYPE.REG_STATE, msg_str);
        m.sendToTarget();
    }

    public void notifyChangeNetwork() {
        Message m = Message.obtain(handler, MSG_TYPE.CHANGE_NETWORK, null);
        m.sendToTarget();
    }

    /* === end of MyAppObserver ==== */
}
